package com.dubilok.dao.impl;

import com.dubilok.connection.GetConnectionImpl;
import com.dubilok.dao.PlayerDao;
import com.dubilok.model.Player;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class PlayerDaoImpl implements PlayerDao {

    private static final String GET_ALL_PLAYERS = "SELECT * FROM player";

    private static final String INSERT_PLAYER = "INSERT INTO player (playerName, playerAge, playerEmail) VALUES (?,?,?)";

    private static final String REMOVE_PLAYER = "DELETE FROM player WHERE id = ?";

    private static final String UPDATE_PLAYER = "UPDATE player SET playerNAme = ? , playerAge = ?, playerEmail = ? WHERE id = ?";

    private Connection connection;

    public PlayerDaoImpl() {
        this.connection = GetConnectionImpl.getInstance().getConnection();
    }

    @Override
    public List<Player> getAllPlayers() {
        List<Player> players = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_PLAYERS);
            while (resultSet.next()) {
                int id = Integer.parseInt(resultSet.getString("id"));
                String name = resultSet.getString("playerName");
                int age = Integer.parseInt(resultSet.getString("playerAge"));
                String email = resultSet.getString("playerEmail");
                Player player = new Player(id, name, age, email);
                players.add(player);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return players;
    }

    @Override
    public boolean insertPlayer(Player player) {
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_PLAYER);
            statement.setString(1, player.getName());
            statement.setInt(2, player.getAge());
            statement.setString(3, player.getEmail());
            statement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean removePlayer(Player player) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(REMOVE_PLAYER);
            preparedStatement.setInt(1, player.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean updatePlayer(Player player) {
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_PLAYER);
            statement.setString(1, player.getName());
            statement.setInt(2, player.getAge());
            statement.setString(3, player.getEmail());
            statement.setInt(4, player.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }
}
