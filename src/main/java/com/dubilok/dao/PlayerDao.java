package com.dubilok.dao;

import com.dubilok.model.Player;

import java.util.List;

public interface PlayerDao {
    List<Player> getAllPlayers();

    boolean insertPlayer(Player player);

    boolean removePlayer(Player player);

    boolean updatePlayer(Player player);
}
