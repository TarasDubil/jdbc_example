package com.dubilok.model;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor()
@EqualsAndHashCode
public class Player {
    private int id;
    private String name;
    private int age;
    private String email;
}

