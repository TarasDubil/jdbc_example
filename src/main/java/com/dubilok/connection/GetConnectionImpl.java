package com.dubilok.connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class GetConnectionImpl implements GetConnection {

    public static GetConnectionImpl instance;

    private Connection connection;

    private GetConnectionImpl() {

    }

    public static GetConnectionImpl getInstance() {
        if (instance == null) {
            instance = new GetConnectionImpl();
        }
        return instance;
    }

    public Connection getConnection() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/main/resources/db.properties"));
            String dbUrl = properties.getProperty("db.url");
            String dbUsername = properties.getProperty("db.username");
            String dbPassword = properties.getProperty("db.password");
            String drivelClassName = properties.getProperty("db.driverClassName");
            Class.forName(drivelClassName);
            this.connection = DriverManager.getConnection(dbUrl,dbUsername,dbPassword);
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return this.connection;
    }

    @Override
    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            //NOP
        }
    }
}
