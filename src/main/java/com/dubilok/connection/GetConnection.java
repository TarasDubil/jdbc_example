package com.dubilok.connection;

import java.sql.Connection;

public interface GetConnection {

    Connection getConnection();

    void closeConnection();
}
