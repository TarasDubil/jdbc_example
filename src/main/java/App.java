import com.dubilok.config.Config;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.SQLException;

public class App {

    public static void main(String[] args) throws SQLException {
        AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(Config.class);

        appContext.close();
    }

}
